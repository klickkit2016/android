package com.klickkit.phantom.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.github.paolorotolo.appintro.util.LogHelper;
import com.klickkit.phantom.R;
import com.klickkit.phantom.adapter.LinksAdapter;
import com.klickkit.phantom.api.APIUtils;
import com.klickkit.phantom.model.Link;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LinksActivity extends AppCompatActivity {
    private static final String TAG = LogHelper.makeLogTag(LinksActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final RecyclerView recyclerView = findViewById(R.id.recycler_links);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String channelName = getIntent().getStringExtra("channel");

        Call<List<Link>> listCall = APIUtils.getPhantomClient().getLinksForChannel(channelName);
        listCall.enqueue(new Callback<List<Link>>() {
            @Override
            public void onResponse(@NonNull Call<List<Link>> call, @NonNull Response<List<Link>> response) {
                LinksAdapter adapter = new LinksAdapter(LinksActivity.this, response.body());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<Link>> call, @NonNull Throwable t) {
                LogHelper.e(TAG, t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
