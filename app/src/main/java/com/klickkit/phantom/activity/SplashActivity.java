package com.klickkit.phantom.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.klickkit.phantom.R;
import com.klickkit.phantom.activity.main.MessagesActivity;
import com.klickkit.phantom.model.ColorLabel;

import io.realm.Realm;

public class SplashActivity extends Activity {
    private static final String TAG = LogHelper.makeLogTag(SplashActivity.class);

    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        settings = getSharedPreferences("settings", 0);
        if (settings.getBoolean("first", true)) {
            firstRunSetup();

            startActivity(new Intent(SplashActivity.this, IntroActivity.class));
        } else if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            startActivity(new Intent(SplashActivity.this, SignInActivity.class));
        } else {
            startActivity(new Intent(SplashActivity.this, MessagesActivity.class));
        }
        finish();
    }

    private void firstRunSetup() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @SuppressWarnings("ResourceType")
            @Override
            public void execute(Realm realm) {
                try {
                    int colors[] = {R.color.md_red_500, R.color.md_blue_500, R.color.md_green_500,
                            R.color.md_yellow_500, R.color.md_grey_500,};
                    for (int color : colors) {
                        ColorLabel colorLabel = realm.createObject(ColorLabel.class, getResources().getColor(color));
                        colorLabel.setName(getResources().getString(color));
                    }
                } catch (Exception e) {
                    LogHelper.e(TAG, e);
                }
            }
        });

        settings.edit().putBoolean("first", false).apply();
    }
}
