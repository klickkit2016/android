package com.klickkit.phantom.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.klickkit.phantom.R;
import com.klickkit.phantom.adapter.LinksAdapter;
import com.klickkit.phantom.adapter.MessageAdapter;
import com.klickkit.phantom.api.APIUtils;
import com.klickkit.phantom.model.Link;
import com.klickkit.phantom.model.Message;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThreadsActivity extends AppCompatActivity {
    private static final String TAG = LogHelper.makeLogTag(ThreadsActivity.class);

    private List<Message> messages = new ArrayList<>();
    private MessageAdapter messageAdapter;
    private RecyclerView rvMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvMessages = findViewById(R.id.recycler_links);
        rvMessages.setLayoutManager(new LinearLayoutManager(this));

        String channelName = getIntent().getStringExtra("channel");

        Call<List<Link>> listCall = APIUtils.getPhantomClient().getLinksForChannel(channelName);
        listCall.enqueue(new Callback<List<Link>>() {
            @Override
            public void onResponse(@NonNull Call<List<Link>> call, @NonNull Response<List<Link>> response) {
                LinksAdapter adapter = new LinksAdapter(ThreadsActivity.this, response.body());
                rvMessages.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<Link>> call, @NonNull Throwable t) {
                LogHelper.e(TAG, t.toString());
            }
        });

        initMessagesAdapter();
        getChannelMessages(channelName);
    }

    private void initMessagesAdapter() {
        messageAdapter = new MessageAdapter(this, messages);
        rvMessages.setAdapter(messageAdapter);
    }

    private void getChannelMessages(String channelId) {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("master/channels/" + channelId + "/messages");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Message message = dataSnapshot.getValue(Message.class);
                message.setId(dataSnapshot.getKey());
                messages.add(message);
                messageAdapter.notifyDataSetChanged();
                rvMessages.smoothScrollToPosition(messageAdapter.getItemCount() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Message changedMessage = dataSnapshot.getValue(Message.class);
                changedMessage.setId(dataSnapshot.getKey());
                for (int i = 0; i < messages.size(); i++) {
                    if (messages.get(i).getId().equals(changedMessage.getId())) {
                        messages.set(i, changedMessage);
                        messageAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Message removedMessage = dataSnapshot.getValue(Message.class);
                removedMessage.setId(dataSnapshot.getKey());
                for (Message message : messages) {
                    if (message.getId().equals(removedMessage.getId())) {
                        messages.remove(message);
                        messageAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "postFirebaseMessages:onCancelled", databaseError.toException());
                Toast.makeText(ThreadsActivity.this, "Failed to load firebaseMessages.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        dbRef.addChildEventListener(childEventListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
