package com.klickkit.phantom.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;
import com.google.firebase.auth.FirebaseAuth;
import com.klickkit.phantom.R;
import com.klickkit.phantom.activity.main.MessagesActivity;

public class IntroActivity extends AppIntro2 {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(AppIntro2Fragment.newInstance("Calendar", "We need to use the Calendar",
                R.mipmap.ic_launcher_round, getResources().getColor(R.color.colorPrimary)));
        askForPermissions(new String[]{Manifest.permission.WRITE_CALENDAR}, 1);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            startActivity(new Intent(IntroActivity.this, SignInActivity.class));
        } else {
            startActivity(new Intent(IntroActivity.this, MessagesActivity.class));
        }
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }
}