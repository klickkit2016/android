package com.klickkit.phantom.activity.main;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;

import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.klickkit.phantom.R;
import com.klickkit.phantom.adapter.DrawerSectionedAdapter;
import com.klickkit.phantom.adapter.MessageAdapter;
import com.klickkit.phantom.adapter.TeamSpinnerAdapter;
import com.klickkit.phantom.model.Channel;
import com.klickkit.phantom.model.Message;
import com.klickkit.phantom.model.Team;

import java.util.ArrayList;
import java.util.List;

public class DrawerActivity extends BaseActivity {
    protected static final String TAG = LogHelper.makeLogTag(DrawerActivity.class);

    protected TeamSpinnerAdapter teamSpinnerAdapter;
    protected DrawerSectionedAdapter drawerSectionedAdapter;

    protected Channel currentChannel;
    protected String currentTeamName;

    protected MessageAdapter messageAdapter;

    protected List<Team> teamList = new ArrayList<>();
    protected List<String> channelNameList = new ArrayList<>();
    protected List<Channel> channelList = new ArrayList<>();
    protected List<Message> messages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        teamSpinnerAdapter = new TeamSpinnerAdapter(this, teamList);
        teamSpinner.setAdapter(teamSpinnerAdapter);
        teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Team currentTeam = (Team) teamSpinner.getItemAtPosition(position);
                currentTeamName = currentTeam.getName();
                channelList.clear();
                drawerSectionedAdapter.notifyDataSetChanged();
                getTeamChannels();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        drawerChannelRV.setLayoutManager(new LinearLayoutManager(this));

        setupDrawerAdapter();
        getUserTeams();
        getTeamChannels();
    }

    private void setupDrawerAdapter() {
        drawerSectionedAdapter = new DrawerSectionedAdapter(this, channelList,
                channelName -> {
                    currentChannelName = channelName;
                    toolbar.setTitle(channelName);
                    drawerLayout.closeDrawer(GravityCompat.START);
                    messageAdapter.clear();

                    for (Channel channel : channelList) {
                        if (channel.getName().equals(channelName)) {
                            currentChannel = channel;
                            getChannelMessages(channel.getName());
                        }
                    }
                });
        drawerSectionedAdapter.shouldShowHeadersForEmptySections(false);
        drawerSectionedAdapter.shouldShowFooters(false);
        drawerChannelRV.setAdapter(drawerSectionedAdapter);
    }

    private void getChannelMessages(String channelName) {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(currentTeamName + "/messages/" + channelName);
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Message message = dataSnapshot.getValue(Message.class);
                message.setId(dataSnapshot.getKey());
                messages.add(message);
                messageAdapter.notifyDataSetChanged();
                rvMessages.smoothScrollToPosition(messageAdapter.getItemCount() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Message changedMessage = dataSnapshot.getValue(Message.class);
                changedMessage.setId(dataSnapshot.getKey());
                for (int i = 0; i < messages.size(); i++) {
                    if (messages.get(i).getId().equals(changedMessage.getId())) {
                        messages.set(i, changedMessage);
                        messageAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Message removedMessage = dataSnapshot.getValue(Message.class);
                removedMessage.setId(dataSnapshot.getKey());
                for (Message message : messages) {
                    if (message.getId().equals(removedMessage.getId())) {
                        messages.remove(message);
                        messageAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        dbRef.addChildEventListener(childEventListener);
    }

    private void getUserTeams() {
        DatabaseReference dbRef = FirebaseDatabase.getInstance()
                .getReference("users/" + currentUser.getUid() + "/teams");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                final String teamName = dataSnapshot.getValue(String.class);
                DatabaseReference teamRef = FirebaseDatabase.getInstance().getReference("teams/" + teamName);
                ValueEventListener valueEventListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Team team = dataSnapshot.getValue(Team.class);

                        //OnTeamRemoved
                        if (team == null) {
                            for (int i = 0; i < teamList.size(); i++) {
                                if (teamList.get(i).getName().equals(teamName)) {
                                    teamList.remove(i);
                                    teamSpinnerAdapter.notifyDataSetChanged();
                                    return;
                                }
                            }
                        } else {
                            //OnTeamChanged
                            for (int i = 0; i < teamList.size(); i++) {
                                if (teamList.get(i).getName().equals(teamName)) {
                                    teamList.set(i, team);
                                    teamSpinnerAdapter.notifyDataSetChanged();
                                    return;
                                }
                            }

                            //OnTeamAdded
                            teamList.add(team);
                            teamSpinnerAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };
                teamRef.addValueEventListener(valueEventListener);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                //Team name cannot be changed
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String teamName = dataSnapshot.getValue(String.class);

                for (int i = 0; i < teamList.size(); i++) {
                    if (teamList.get(i).getName().equals(teamName)) {
                        teamList.remove(i);
                        teamSpinnerAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        dbRef.addChildEventListener(childEventListener);
    }

    private void getTeamChannels() {
        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance()
                .getReference(currentTeamName + "/channels");
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference(currentTeamName + "/users/" + currentUser.getUid() + "/channels");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                String channelName = dataSnapshot.getValue(String.class);
                channelNameList.add(channelName);

                Query query = databaseReference1.child(channelName);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String name = "", dpURL = "";
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                if (issue.getKey().equals("name")) {
                                    name = issue.getValue(String.class);
                                }

                                if (issue.getKey().equals("dpURL")) {
                                    dpURL = issue.getValue(String.class);
                                }
                            }
                        }

                        if (!name.isEmpty()) {
                            channelList.add(new Channel(name, dpURL));
                            drawerSectionedAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                String changedChannelName = dataSnapshot.getValue(String.class);
                for (int i = 0; i < channelNameList.size(); i++) {
                    if (channelNameList.get(i).equals(changedChannelName)) {
                        channelNameList.set(i, changedChannelName);
                        drawerSectionedAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        databaseReference.addChildEventListener(childEventListener);
    }
}
