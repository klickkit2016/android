package com.klickkit.phantom.activity.main;

import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;

import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.klickkit.phantom.R;
import com.klickkit.phantom.adapter.MessageAdapter;
import com.klickkit.phantom.adapter.mentions.MentionsAdapter;
import com.klickkit.phantom.adapter.mentions.RecyclerItemClickListener;
import com.klickkit.phantom.api.APIUtils;
import com.klickkit.phantom.model.ColorLabel;
import com.klickkit.phantom.model.Mention;
import com.klickkit.phantom.model.Message;
import com.klickkit.phantom.model.User;
import com.klickkit.phantom.utils.MentionsLoaderUtils;
import com.klickkit.phantom.utils.MessageInput;
import com.percolate.caffeine.ViewUtils;
import com.percolate.mentions.Mentionable;
import com.percolate.mentions.Mentions;
import com.percolate.mentions.QueryListener;
import com.percolate.mentions.SuggestionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import io.realm.Realm;

public class MessagesActivity extends DrawerActivity implements MessageInput.InputListener,
        QueryListener, SuggestionsListener, ColorChooserDialog.ColorCallback {
    private static final String TAG = LogHelper.makeLogTag(MessagesActivity.class);

    protected Realm realm;

    protected Mentions mentions;
    protected MentionsAdapter mentionsAdapter;
    protected MentionsLoaderUtils mentionsLoaderUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rvMessages.setLayoutManager(new LinearLayoutManager(this));

        initMessagesAdapter();

        messageInput.setInputListener(this);

        initMentions();
        setupMentionsList();

        realm = Realm.getDefaultInstance();
    }

    /**
     * ChatKit
     */
    @Override
    public boolean onSubmit(final CharSequence input) {
        StringBuilder inputStringBuilder = new StringBuilder(input.toString());
        final List<Mentionable> mentionList = mentions.getInsertedMentions();
        for (int i = 0; i < mentionList.size(); i++) {
            inputStringBuilder.insert(mentionList.get(i).getMentionOffset() + i, '@');
        }

        Message message = new Message(currentUser.getUid(), currentUser.getDisplayName(),
                inputStringBuilder.toString(), String.valueOf(new Date().getTime()));

        if (currentChannel != null && currentChannel.getName() != null && currentChannel.getName().equals(currentChannelName)) {
            String key = FirebaseDatabase.getInstance().getReference().push().getKey();
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(currentTeamName + "/messages/"
                    + currentChannel.getName() + "/" + key);
            dbRef.setValue(message);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("channel_name", currentChannelName);
            jsonObject.put("email", currentUser.getEmail());
            jsonObject.put("message", inputStringBuilder.toString());
        } catch (JSONException je) {
            LogHelper.e(TAG, je.toString());
        }

        AndroidNetworking.post(APIUtils.PHANTOM_BASE_URL + "submitMessage")
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Message phantomResponse = new Message("Phantom", "Phantom",
                                    response.getString("speech_response"),
                                    String.valueOf(new Date().getTime()));

                            if (currentChannel != null && currentChannel.getName() != null && currentChannel.getName().equals(currentChannelName)) {
                                String key = FirebaseDatabase.getInstance().getReference().push().getKey();
                                DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(currentTeamName + "/messages/"
                                        + currentChannel.getName() + "/" + key);
                                dbRef.setValue(phantomResponse);
                            }
                        } catch (JSONException je) {
                            LogHelper.e(TAG, je.toString());
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        LogHelper.e(TAG, error.getErrorCode(), error.getErrorDetail(), error.getErrorBody(), error.getResponse());
                    }
                });

        return true;
    }

    private void initMessagesAdapter() {
        messageAdapter = new MessageAdapter(this, messages);
        rvMessages.setAdapter(messageAdapter);
    }

    /**
     * Mentions
     */
    private void initMentions() {
        mentions = new Mentions.Builder(this, messageInput.getInputEditText())
                .suggestionsListener(this)
                .queryListener(this)
                .build();
        mentionsLoaderUtils = new MentionsLoaderUtils(this);
    }

    private void setupMentionsList() {
        mentionsListRV.setLayoutManager(new LinearLayoutManager(this));
        mentionsAdapter = new MentionsAdapter(this);
        mentionsListRV.setAdapter(mentionsAdapter);

        mentionsListRV.addOnItemTouchListener(new RecyclerItemClickListener(this, (view, position) -> {
            final User user = mentionsAdapter.getItem(position);
            if (user != null) {
                final Mention mention = new Mention();
                mention.setMentionName(user.getName());
                mentions.insertMention(mention);
            }
        }));
    }

    @Override
    public void onQueryReceived(String query) {
        final List<User> users = mentionsLoaderUtils.searchUsers(query);
        if (users != null && !users.isEmpty()) {
            mentionsAdapter.clear();
            mentionsAdapter.setCurrentQuery(query);
            mentionsAdapter.addAll(users);
            showMentionsList(true);
        } else {
            showMentionsList(false);
        }
    }

    @Override
    public void displaySuggestions(boolean display) {
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list_layout);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list_layout);
        }
    }

    private void showMentionsList(boolean display) {
        ViewUtils.showView(this, R.id.mentions_list_layout);
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list);
            ViewUtils.hideView(this, R.id.mentions_empty_view);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list);
            ViewUtils.showView(this, R.id.mentions_empty_view);
        }
    }

    /**
     * ColorChooserDialog
     */
    @Override
    public void onColorSelection(@NonNull final ColorChooserDialog dialog, @ColorInt final int selectedColor) {
        final DatabaseReference dbRef = FirebaseDatabase.getInstance()
                .getReference(currentTeamName + "/messages/" + currentChannel.getName() + "/" + dialog.tag());
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Message message = dataSnapshot.getValue(Message.class);
                message.setColorLabelHex("#" + Integer.toHexString(selectedColor));
                dbRef.removeEventListener(this);
                dbRef.setValue(message);
                for (int i = messages.size() - 1; i >= 0; i--) {
                    if (messages.get(i).getId().equals(dialog.tag())) {
                        messages.get(i).setColorLabelHex("#" + Integer.toHexString(selectedColor));
                        messageAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        realm.executeTransaction(realm -> {
            try {
                ColorLabel colorLabel = realm.createObject(ColorLabel.class, selectedColor);
                colorLabel.setName("#" + Integer.toHexString(selectedColor));
            } catch (Exception e) {
                LogHelper.e(TAG, e);
            }
        });
    }

    @Override
    public void onColorChooserDismissed(@NonNull ColorChooserDialog dialog) {
    }
}
