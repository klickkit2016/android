package com.klickkit.phantom.activity.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;

import com.appsee.Appsee;
import com.crashlytics.android.Crashlytics;
import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.klickkit.phantom.R;
import com.klickkit.phantom.activity.LinksActivity;
import com.klickkit.phantom.activity.SignInActivity;
import com.klickkit.phantom.activity.ThreadsActivity;
import com.klickkit.phantom.utils.MessageInput;

import io.fabric.sdk.android.Fabric;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    protected static final String TAG = LogHelper.makeLogTag(BaseActivity.class);

    public static String currentChannelName;

    protected FirebaseUser currentUser;

    protected Toolbar toolbar;
    protected DrawerLayout drawerLayout;
    protected RecyclerView drawerChannelRV;
    protected RecyclerView rvMessages;
    protected RecyclerView mentionsListRV;
    protected MessageInput messageInput;
    protected Spinner teamSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        Appsee.start(getString(R.string.com_appsee_apikey));

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser == null) {
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        }

        setContentView(R.layout.activity_main);
        findViewByIds();
        setSupportActionBar(toolbar);
    }

    private void findViewByIds() {
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        teamSpinner = findViewById(R.id.spinner_main_teams);
        drawerChannelRV = findViewById(R.id.rv_drawer_channels);
        rvMessages = findViewById(R.id.rv_messages);
        mentionsListRV = findViewById(R.id.mentions_list);
        messageInput = findViewById(R.id.message_input);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pla:
                startActivity(new Intent(this, LinksActivity.class)
                        .putExtra("channel", currentChannelName));
                break;

            case R.id.action_threads:
                startActivity(new Intent(this, ThreadsActivity.class)
                        .putExtra("channel", currentChannelName));
                break;

//            TODO
//            case R.id.action_copy:
//                messagesAdapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
//                Toast.makeText(this, "Message Copied.", Toast.LENGTH_SHORT).show();
//                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
