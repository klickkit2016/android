package com.klickkit.phantom.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.klickkit.phantom.R;

public class MessageThreadActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvMessageThreadInChannel;
    private TextView messageTime;
    private TextView messageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_thread);
        findViewByIds();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvMessageThreadInChannel.setText(String.format("Message in #%s", getIntent().getStringExtra("channel")));
        messageTime.setText(getIntent().getStringExtra("time"));
        messageText.setText(getIntent().getStringExtra("message"));
    }

    private void findViewByIds() {
        toolbar = findViewById(R.id.toolbar);
        tvMessageThreadInChannel = findViewById(R.id.tv_message_thread_in_channel);
        messageTime = findViewById(R.id.messageTime);
        messageText = findViewById(R.id.messageText);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
