package com.klickkit.phantom.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.klickkit.phantom.R;
import com.klickkit.phantom.activity.main.MessagesActivity;
import com.klickkit.phantom.adapter.CustomListAdpater;
import com.klickkit.phantom.api.APIUtils;
import com.klickkit.phantom.model.Channel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TasksActivity extends AppCompatActivity {
    private static final String TAG = LogHelper.makeLogTag(TasksActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<String> sections = new ArrayList<>();

        sections.add("UI");
        sections.add("Backend");

        ListView listView = findViewById(R.id.listview_tasks);

        CustomListAdpater customListAdpater = new CustomListAdpater(this, R.layout.task_listview, sections);
        listView.setAdapter(customListAdpater);

        String projectName = getIntent().getStringExtra("projectName");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject("{\n" +
                    "\t\"channel_name\": \"" + projectName + "\", \n" +
                    "\t\"employees\": [\n" +
                    "\t\t{\n" +
                    "\t\t\t\"role_name\": \"AD\", \n" +
                    "\t\t\t\"employee\": {\n" +
                    "\t\t\t\t\"name\": \"sricharanR\", \n" +
                    "\t\t\t\t\"priority\": \"1\", \n" +
                    "\t\t\t\t\"email\": \"sricharan14312@cse.ssn.edu.in\"\n" +
                    "\t\t\t}\n" +
                    "\t\t}, \n" +
                    "\t\t{\n" +
                    "\t\t\t\"role_name\": \"AD\", \n" +
                    "\t\t\t\"employee\": {\n" +
                    "\t\t\t\t\"name\": \"SricharanPrograms\", \n" +
                    "\t\t\t\t\"priority\": \"1\", \n" +
                    "\t\t\t\t\"email\": \"sricharanprograms@gmail.com\"\n" +
                    "\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t], \n" +
                    "\t\"manager_email\": \"sricharan14312@cse.ssn.edu.in\", \n" +
                    "\t\"tasks\": [\n" +
                    "\t\t{\n" +
                    "\t\t\t\"task_name\": \"UI\", \n" +
                    "\t\t\t\"sub_tasks\": []\n" +
                    "\t\t}, \n" +
                    "\t\t{\n" +
                    "\t\t\t\"task_name\": \"Backend\", \n" +
                    "\t\t\t\"sub_tasks\": []\n" +
                    "\t\t}\n" +
                    "\t]\n" +
                    "}");
        } catch (JSONException je) {
            LogHelper.e(TAG, je);
        }

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        String key = databaseReference.push().getKey();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("master/channels/" + key);
        Channel channel = new Channel();
        channel.setName(projectName);
        dbRef.setValue(channel);

        //TODO
//        AssociatedRoom associatedRoom = new AssociatedRoom(key);
//        key = databaseReference.push().getKey();
//        DatabaseReference dbRef2 = FirebaseDatabase.getInstance().getReference("master/sricharanprogramsgmailcom/associated_rooms/" + key);
//        dbRef2.setValue(associatedRoom);

        AndroidNetworking.post(APIUtils.PHANTOM_BASE_URL + "submitTasks")
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        LogHelper.e(TAG, response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        LogHelper.e(TAG, error.toString());
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_done) {
            startActivity(new Intent(this, MessagesActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
