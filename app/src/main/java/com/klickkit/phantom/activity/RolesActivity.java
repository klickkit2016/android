package com.klickkit.phantom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.github.paolorotolo.appintro.util.LogHelper;
import com.klickkit.phantom.R;
import com.klickkit.phantom.adapter.RolesSectionedAdapter;
import com.klickkit.phantom.api.APIUtils;
import com.klickkit.phantom.model.Employee;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RolesActivity extends AppCompatActivity {
    private static final String TAG = LogHelper.makeLogTag(RolesActivity.class);

    public Context context;
    public Intent intent;
    private List<String> sections = new ArrayList<>();
    private List<List<Employee>> employeeListList = new ArrayList<>();
    private String[] roleIds = {"AD", "BD", "DS", "ID", "WFD"};
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roles);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        sections.add("Android Developer");
        sections.add("Backend Developer");
        sections.add("Designer");
        sections.add("iOS Developer");
        sections.add("Web Frontend Developer");

        getEmployeesForRoles(0);
    }

    private void getEmployeesForRoles(final int i) {
        Call<List<Employee>> listCall = APIUtils.getPhantomClient().getEmployeesForRole(roleIds[i]);
        listCall.enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(@NonNull Call<List<Employee>> call, @NonNull Response<List<Employee>> response) {
                List<Employee> employeeList = response.body();
                employeeListList.add(employeeList);
                if (i + 1 < roleIds.length) {
                    getEmployeesForRoles(i + 1);
                } else {
                    initSectionedAdapter();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Employee>> call, @NonNull Throwable t) {
                LogHelper.e(TAG, t.toString());
            }
        });
    }

    private void initSectionedAdapter() {
        RolesSectionedAdapter rolesSectionedAdapter = new RolesSectionedAdapter(RolesActivity.this,
                sections, employeeListList, (sectionHolder, section, relativePosition) -> sectionHolder.devName.toggle());
        rolesSectionedAdapter.shouldShowHeadersForEmptySections(false);
        rolesSectionedAdapter.shouldShowFooters(false);
        recyclerView.setAdapter(rolesSectionedAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_done) {
            startActivity(new Intent(RolesActivity.this, TasksActivity.class)
                    .putExtra("projectName", getIntent().getStringExtra("projectName")));
        }
        return super.onOptionsItemSelected(item);
    }
}
