package com.klickkit.phantom.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.klickkit.phantom.R;
import com.klickkit.phantom.utils.SublimePickerFragment;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;

public class CreateProjectActivity extends AppCompatActivity {

    private static final int SELECT_PHOTO = 100;
    public Uri imageUri;
    SelectedDate mSelectedDate;
    int mHour, mMinute;
    private Toolbar toolbar;
    private ImageView ivProjectImage;
    private Button btnProjectDeadline;
    SublimePickerFragment.Callback mFragmentCallback = new SublimePickerFragment.Callback() {
        @Override
        public void onCancelled() {
        }

        @Override
        public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute,
                                            SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                            String recurrenceRule) {

            mSelectedDate = selectedDate;
            mHour = hourOfDay;
            mMinute = minute;

            btnProjectDeadline.setText(selectedDate.toString());

            String dateTimeString = DateFormat.getDateInstance().format(selectedDate.getStartDate().getTime()) + "   ";
            if (String.valueOf(mHour).length() == 2) {
                dateTimeString += mHour + ":";
            } else {
                dateTimeString += "0" + mHour + ":";
            }
            if (String.valueOf(mMinute).length() == 2) {
                dateTimeString += mMinute;
            } else {
                dateTimeString += "0" + mMinute;
            }

            btnProjectDeadline.setText(dateTimeString);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_project);
        findViewByIds();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ivProjectImage.setOnClickListener(view -> {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        });

        btnProjectDeadline.setOnClickListener(v -> {
            SublimePickerFragment pickerFrag = new SublimePickerFragment();
            pickerFrag.setCallback(mFragmentCallback);

            Pair<Boolean, SublimeOptions> optionsPair = getOptions();

            Bundle bundle = new Bundle();
            bundle.putParcelable("SUBLIME_OPTIONS", optionsPair.second);
            pickerFrag.setArguments(bundle);

            pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            pickerFrag.show(getSupportFragmentManager(), "SUBLIME_PICKER");
        });
    }

    private void findViewByIds() {
        toolbar = findViewById(R.id.toolbar);
        ivProjectImage = findViewById(R.id.iv_project_image);
        btnProjectDeadline = findViewById(R.id.btn_project_deadline);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    imageUri = selectedImage;
                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    ivProjectImage.setImageURI(selectedImage);
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_done) {
            Intent intent = new Intent(this, RolesActivity.class);
            intent.putExtra("image", imageUri);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    Pair<Boolean, SublimeOptions> getOptions() {
        SublimeOptions options = new SublimeOptions();

        int displayOptions = 0;
        displayOptions |= SublimeOptions.ACTIVATE_DATE_PICKER;
        displayOptions |= SublimeOptions.ACTIVATE_TIME_PICKER;

        options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(displayOptions);
        options.setCanPickDateRange(false);

        return new Pair<>(displayOptions != 0 ? Boolean.TRUE : Boolean.FALSE, options);
    }
}
