package com.klickkit.phantom.model;

public class Link {
    private String link_name;
    private String url;

    public Link() {
    }

    public Link(String link_name, String url) {
        this.link_name = link_name;
        this.url = url;
    }

    public String getLink_name() {
        return link_name;
    }

    public void setLink_name(String link_name) {
        this.link_name = link_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Link{" +
                "link_name='" + link_name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
