package com.klickkit.phantom.model;

public class Message {
    private String id;
    private String senderId;
    private String senderName;
    private String text;
    private String createdAt;
    private String colorLabelHex;

    public Message() {
    }


    public Message(String senderId, String senderName, String text) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.text = text;
        this.createdAt = String.valueOf(System.currentTimeMillis());
    }

    public Message(String senderId, String senderName, String text, String createdAt) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.text = text;
        this.createdAt = createdAt;
    }

    public Message(String senderId, String senderName, String text, String createdAt, String colorLabelHex) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.text = text;
        this.createdAt = createdAt;
        this.colorLabelHex = colorLabelHex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getColorLabelHex() {
        return colorLabelHex;
    }

    public void setColorLabelHex(String colorLabelHex) {
        this.colorLabelHex = colorLabelHex;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", senderId='" + senderId + '\'' +
                ", senderName='" + senderName + '\'' +
                ", text='" + text + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", colorLabelHex='" + colorLabelHex + '\'' +
                '}';
    }
}
