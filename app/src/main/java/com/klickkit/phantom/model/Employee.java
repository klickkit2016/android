package com.klickkit.phantom.model;

import java.util.List;

/**
 * Created by adithya321 on 8/20/17.
 */

public class Employee {
    private User user;
    private List<Priority> priority;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Priority> getPriority() {
        return priority;
    }

    public void setPriority(List<Priority> priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "user=" + user +
                ", priority=" + priority +
                '}';
    }

    public static class User {
        private String username;
        private String email;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public String toString() {
            return "User{" +
                    "username='" + username + '\'' +
                    ", email='" + email + '\'' +
                    '}';
        }
    }

    public static class Priority {
        private int magnitude;

        public int getMagnitude() {
            return magnitude;
        }

        public void setMagnitude(int magnitude) {
            this.magnitude = magnitude;
        }

        @Override
        public String toString() {
            return "Priority{" +
                    "magnitude=" + magnitude +
                    '}';
        }
    }
}
