package com.klickkit.phantom.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by adithya321 on 8/16/17.
 */

public class ColorLabel extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;

    public ColorLabel() {
    }

    public ColorLabel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ColorLabel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
