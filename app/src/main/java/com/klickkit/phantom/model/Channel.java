package com.klickkit.phantom.model;

public class Channel {
    private String name;
    private String dpURL;
    private String purpose;
    private String deadline;

    public Channel() {
    }

    public Channel(String name, String dpURL) {
        this.name = name;
        this.dpURL = dpURL;
    }

    public Channel(String name, String dpURL, String purpose, String deadline) {
        this.name = name;
        this.dpURL = dpURL;
        this.purpose = purpose;
        this.deadline = deadline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDpURL() {
        return dpURL;
    }

    public void setDpURL(String dpURL) {
        this.dpURL = dpURL;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "name='" + name + '\'' +
                ", dpURL='" + dpURL + '\'' +
                ", purpose='" + purpose + '\'' +
                ", deadline='" + deadline + '\'' +
                '}';
    }
}
