package com.klickkit.phantom.model;

import io.realm.annotations.PrimaryKey;

public class Team {
    @PrimaryKey
    private String name;
    private String dpURL;

    public Team() {
    }

    public Team(String name, String dpURL) {
        this.name = name;
        this.dpURL = dpURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDpURL() {
        return dpURL;
    }

    public void setDpURL(String dpURL) {
        this.dpURL = dpURL;
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", dpURL='" + dpURL + '\'' +
                '}';
    }
}
