package com.klickkit.phantom.model;

/**
 * Created by sundararaman on 29/7/17.
 */

public class Dev {
    private String name;
    private int level;

    public Dev(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
