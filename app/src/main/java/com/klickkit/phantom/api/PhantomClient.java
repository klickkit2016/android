package com.klickkit.phantom.api;

import com.klickkit.phantom.model.Employee;
import com.klickkit.phantom.model.Link;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PhantomClient {

    @GET("getEmployees/{role}")
    Call<List<Employee>> getEmployeesForRole(@Path("role") String role);

    @GET("listLinks/{channel}")
    Call<List<Link>> getLinksForChannel(@Path("channel") String channel);
}
