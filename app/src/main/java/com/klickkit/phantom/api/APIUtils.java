package com.klickkit.phantom.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by adithya321 on 8/19/17.
 */

public class APIUtils {
    public static final String PHANTOM_BASE_URL = "http://192.168.1.102:8000/phantom/";
    private static final String FIREBASE_BASE_URL = "https://phantom-gab-engine.firebaseio.com/";

    public static FirebaseClient getFirebaseClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FIREBASE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(FirebaseClient.class);
    }

    public static PhantomClient getPhantomClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PHANTOM_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        return retrofit.create(PhantomClient.class);
    }
}
