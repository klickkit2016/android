package com.klickkit.phantom.api;

import com.klickkit.phantom.model.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FirebaseClient {

    @GET("users/{uid}.json")
    Call<User> getUserDetails(@Path("uid") String uid);
}
