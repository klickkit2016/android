package com.klickkit.phantom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;
import com.github.paolorotolo.appintro.util.LogHelper;
import com.klickkit.phantom.R;
import com.klickkit.phantom.model.Employee;

import java.util.List;

/**
 * Created by sundararaman on 29/7/17.
 */

public class RolesSectionedAdapter extends SectionedRecyclerViewAdapter<RolesSectionedAdapter.SectionHolder> {
    private static final String TAG = LogHelper.makeLogTag(RolesSectionedAdapter.class);

    public RecyclerTouchListenerHeader recyclerTouchHeader;
    private Context context;
    private List<String> sections;
    private List<List<Employee>> employeeListList;

    public RolesSectionedAdapter(Context context, List<String> sections, List<List<Employee>> employeeListList,
                                 RecyclerTouchListenerHeader recyclerTouchHeader) {
        this.context = context;
        this.sections = sections;
        this.employeeListList = employeeListList;
        this.recyclerTouchHeader = recyclerTouchHeader;
    }

    @Override
    public int getSectionCount() {
        return sections.size();
    }

    @Override
    public int getItemCount(int section) {
        return employeeListList.get(section).size();
    }

    @Override
    public void onBindHeaderViewHolder(RolesSectionedAdapter.SectionHolder holder, int section, boolean expanded) {
        holder.devLevel.setText(sections.get(section));
    }

    @Override
    public void onBindFooterViewHolder(RolesSectionedAdapter.SectionHolder holder, int section) {

    }

    @Override
    public void onBindViewHolder(final RolesSectionedAdapter.SectionHolder holder, final int section,
                                 final int relativePosition, int absolutePosition) {
        Employee employee = employeeListList.get(section).get(relativePosition);
        holder.devName.setText(employee.getUser().getUsername());
        holder.devLevel.setText("Level " + employee.getPriority().get(0).getMagnitude());
        holder.layout.setOnClickListener(v -> recyclerTouchHeader.onClick(holder, section, relativePosition));
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        if (section == 1) {
            return 0; // VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1. You can return 0 or greater.
        }
        return super.getItemViewType(section, relativePosition, absolutePosition);
    }

    @Override
    public RolesSectionedAdapter.SectionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.item_role_header;
                break;
            case VIEW_TYPE_ITEM:
                layout = R.layout.item_role;
                break;
            default:
                layout = R.layout.item_role;
        }
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new SectionHolder(v, this);
    }

    public interface RecyclerTouchListenerHeader {
        void onClick(SectionHolder sectionHolder, int section, int relativePosition);
    }

    public static class SectionHolder extends SectionedViewHolder implements View.OnClickListener {
        final public CheckedTextView devName;
        final LinearLayout layout;
        final TextView devLevel;
        final RolesSectionedAdapter rolesSectionedAdapter;

        SectionHolder(View view, RolesSectionedAdapter rolesSectionedAdapter) {
            super(view);
            this.layout = view.findViewById(R.id.role_view);
            this.devName = view.findViewById(R.id.dev_name);
            this.devLevel = view.findViewById(R.id.dev_level);
            this.rolesSectionedAdapter = rolesSectionedAdapter;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (isHeader()) {
                rolesSectionedAdapter.toggleSectionExpanded(getRelativePosition().section());
            }
        }
    }
}
