package com.klickkit.phantom.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.paolorotolo.appintro.util.LogHelper;
import com.klickkit.phantom.R;
import com.klickkit.phantom.activity.CreateProjectActivity;
import com.klickkit.phantom.model.Channel;

import java.util.List;

public class DrawerSectionedAdapter extends SectionedRecyclerViewAdapter<DrawerSectionedAdapter.DrawerViewHolder> {
    private static final String TAG = LogHelper.makeLogTag(DrawerSectionedAdapter.class);

    private Context context;
    private RecyclerTouchListenerHeader recyclerTouchListenerHeader;

    private String[] sections = {"Channels", "Direct Messages"};
    private List<Channel> channelList;


    public DrawerSectionedAdapter(Context context, List<Channel> channelList,
                                  RecyclerTouchListenerHeader recyclerTouchListenerHeader) {
        this.context = context;
        this.channelList = channelList;
        this.recyclerTouchListenerHeader = recyclerTouchListenerHeader;
    }

    @Override
    public int getSectionCount() {
        return sections.length;
    }

    @Override
    public int getItemCount(int section) {
        return channelList.size();
    }

    @Override
    public void onBindHeaderViewHolder(DrawerViewHolder holder, final int section, boolean expanded) {
        holder.title.setText(sections[section]);
        holder.caret.setImageResource(expanded ? R.drawable.ic_collapse : R.drawable.ic_expand);
        holder.add.setOnClickListener(v -> {
            switch (section) {
                case 0:
                default:
                    context.startActivity(new Intent(context, CreateProjectActivity.class));
            }
        });
    }

    @Override
    public void onBindFooterViewHolder(DrawerViewHolder holder, int section) {
    }

    @Override
    public void onBindViewHolder(final DrawerViewHolder holder, final int section,
                                 int relativePosition, int absolutePosition) {
        switch (section) {
            case 0:
            case 1:
                final Channel channel = channelList.get(relativePosition);

                holder.title.setText(channel.getName());
                if (channel.getDpURL() != null && !channel.getDpURL().isEmpty())
                    Glide.with(context).load(channel.getDpURL())
                            .apply(RequestOptions.circleCropTransform())
                            .into(holder.dpIV);

                holder.title.setOnClickListener(v -> recyclerTouchListenerHeader.onClick(channel.getName()));
        }
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        if (section == 1) {
            return 0; // VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1. You can return 0 or greater.
        }
        return super.getItemViewType(section, relativePosition, absolutePosition);
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.item_drawer_header;
                break;

            case VIEW_TYPE_ITEM:
                layout = R.layout.item_drawer_item;
                break;

            default:
                layout = R.layout.item_drawer_item;
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new DrawerViewHolder(v, this);
    }

    public interface RecyclerTouchListenerHeader {
        void onClick(String channelName);
    }

    static class DrawerViewHolder extends SectionedViewHolder implements View.OnClickListener {
        final TextView title;
        final ImageView caret;
        final ImageView add;
        final ImageView dpIV;
        final DrawerSectionedAdapter adapter;

        DrawerViewHolder(View itemView, DrawerSectionedAdapter adapter) {
            super(itemView);
            this.title = itemView.findViewById(R.id.tv_drawer_item_title);
            this.caret = itemView.findViewById(R.id.caret);
            this.add = itemView.findViewById(R.id.add);
            this.dpIV = itemView.findViewById(R.id.iv_drawer_item_icon);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (isHeader()) {
                adapter.toggleSectionExpanded(getRelativePosition().section());
            }
        }
    }
}
