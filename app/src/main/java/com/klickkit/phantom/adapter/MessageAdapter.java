package com.klickkit.phantom.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.github.paolorotolo.appintro.util.LogHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.klickkit.phantom.R;
import com.klickkit.phantom.activity.MessageThreadActivity;
import com.klickkit.phantom.model.ColorLabel;
import com.klickkit.phantom.model.Message;
import com.klickkit.phantom.utils.DateFormatter;

import java.util.Date;
import java.util.List;

import io.realm.Realm;

import static com.klickkit.phantom.activity.main.DrawerActivity.currentChannelName;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = LogHelper.makeLogTag(MessageAdapter.class);
    private final static int TYPE_INCOMING = 0, TYPE_OUTGOING = 1;

    private List<Message> messages;
    private Context context;

    public MessageAdapter(Context context, List<Message> messages) {
        this.messages = messages;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        switch (viewType) {
            case 0:
                View incomingMessageView = inflater.inflate(R.layout.item_incoming_text_message, parent, false);
                return new IncomingViewHolder(incomingMessageView);
            case 1:
                View outgoingMessageView = inflater.inflate(R.layout.item_outgoing_text_message, parent, false);
                return new OutgoingViewHolder(outgoingMessageView);

            default:
                View defaultMessageView = inflater.inflate(R.layout.item_incoming_text_message, parent, false);
                return new IncomingViewHolder(defaultMessageView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Message message = messages.get(position);
        int viewType = viewHolder.getItemViewType();

        switch (viewType) {
            case TYPE_INCOMING:
                if (message.getCreatedAt() != null && !message.getCreatedAt().isEmpty()) {
                    ((IncomingViewHolder) viewHolder).messageTime.setText(DateFormatter.format(new Date(Long.valueOf(message.getCreatedAt())),
                            DateFormatter.Template.TIME));
                }
                ((IncomingViewHolder) viewHolder).messageText.setText(message.getText());

                if (message.getColorLabelHex() != null) {
                    ((IncomingViewHolder) viewHolder).messageLL.setBackgroundColor(Color.parseColor(message.getColorLabelHex()));
                }

                ((IncomingViewHolder) viewHolder).messageLL.setOnClickListener(v -> {
                    Intent intent = new Intent(context, MessageThreadActivity.class);
                    intent.putExtra("channel", currentChannelName);
                    intent.putExtra("message", message.getText());
                    if (message.getCreatedAt() != null && !message.getCreatedAt().isEmpty()) {
                        intent.putExtra("time", DateFormatter.format(new Date(Long.valueOf(message.getCreatedAt())),
                                DateFormatter.Template.TIME));
                    }
                    context.startActivity(intent);
                });
                ((IncomingViewHolder) viewHolder).messageLL.setOnLongClickListener(v -> {
                    Realm realm = Realm.getDefaultInstance();
                    List<ColorLabel> colorLabels = realm.where(ColorLabel.class).findAll();
                    int[] primary = new int[colorLabels.size()];
                    for (int i = 0; i < colorLabels.size(); i++) {
                        primary[i] = colorLabels.get(i).getId();
                    }
                    int[][] secondary = new int[][]{};
                    new ColorChooserDialog.Builder(context, R.string.color_palette)
                            .titleSub(R.string.colors)
                            .doneButton(R.string.md_done_label)
                            .cancelButton(R.string.md_cancel_label)
                            .backButton(R.string.md_back_label)
                            .customColors(primary, secondary)
                            .dynamicButtonColor(true)
                            .tag(message.getId())
                            .show((FragmentActivity) context);
                    return true;
                });
                break;

            case TYPE_OUTGOING:
                if (message.getCreatedAt() != null && !message.getCreatedAt().isEmpty()) {
                    ((OutgoingViewHolder) viewHolder).messageTime.setText(DateFormatter.format(new Date(Long.valueOf(message.getCreatedAt())),
                            DateFormatter.Template.TIME));
                }
                ((OutgoingViewHolder) viewHolder).messageText.setText(message.getText());

                if (message.getColorLabelHex() != null) {
                    ((OutgoingViewHolder) viewHolder).messageLL.setBackgroundColor(Color.parseColor(message.getColorLabelHex()));
                }

                ((OutgoingViewHolder) viewHolder).messageLL.setOnClickListener(v -> {
                    Intent intent = new Intent(context, MessageThreadActivity.class);
                    intent.putExtra("channel", currentChannelName);
                    intent.putExtra("message", message.getText());
                    if (message.getCreatedAt() != null && !message.getCreatedAt().isEmpty()) {
                        intent.putExtra("time", DateFormatter.format(new Date(Long.valueOf(message.getCreatedAt())),
                                DateFormatter.Template.TIME));
                    }
                    context.startActivity(intent);
                });
                ((OutgoingViewHolder) viewHolder).messageLL.setOnLongClickListener(v -> {
                    Realm realm = Realm.getDefaultInstance();
                    List<ColorLabel> colorLabels = realm.where(ColorLabel.class).findAll();
                    int[] primary = new int[colorLabels.size()];
                    for (int i = 0; i < colorLabels.size(); i++) {
                        primary[i] = colorLabels.get(i).getId();
                    }
                    int[][] secondary = new int[][]{};
                    new ColorChooserDialog.Builder(context, R.string.color_palette)
                            .titleSub(R.string.colors)
                            .doneButton(R.string.md_done_label)
                            .cancelButton(R.string.md_cancel_label)
                            .backButton(R.string.md_back_label)
                            .customColors(primary, secondary)
                            .dynamicButtonColor(true)
                            .tag(message.getId())
                            .show((FragmentActivity) context);
                    return true;
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        String senderId = messages.get(position).getSenderId();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser != null && senderId.equals(currentUser.getUid())) {
            return TYPE_OUTGOING;
        } else {
            return TYPE_INCOMING;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void clear() {
        messages.clear();
        notifyDataSetChanged();
    }

    public class IncomingViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout messageLL;
        private TextView messageTime;
        private TextView messageText;

        IncomingViewHolder(View itemView) {
            super(itemView);

            messageLL = itemView.findViewById(R.id.ll_incoming_message);
            messageTime = itemView.findViewById(R.id.messageTime);
            messageText = itemView.findViewById(R.id.messageText);
        }
    }

    public class OutgoingViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout messageLL;
        private TextView messageTime;
        private TextView messageText;

        OutgoingViewHolder(View itemView) {
            super(itemView);

            messageLL = itemView.findViewById(R.id.ll_outgoing_message);
            messageTime = itemView.findViewById(R.id.messageTime);
            messageText = itemView.findViewById(R.id.messageText);
        }
    }
}