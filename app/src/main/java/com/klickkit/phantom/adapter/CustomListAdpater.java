package com.klickkit.phantom.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.klickkit.phantom.R;

import java.util.List;

/**
 * Created by sundararaman on 30/7/17.
 */

public class CustomListAdpater extends ArrayAdapter<String> {

    private Context context;
    private List<String> objects;

    public CustomListAdpater(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
        super(context, resource, objects);

        this.context = context;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.task_listview, null);

        final CheckedTextView title = view.findViewById(R.id.task_title);
        title.setText(objects.get(position));

        title.setOnClickListener(v -> {
            if (title.isChecked())
                title.setChecked(false);
            else
                title.setChecked(true);
        });

        return view;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
