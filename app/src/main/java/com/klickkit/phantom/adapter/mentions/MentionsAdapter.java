package com.klickkit.phantom.adapter.mentions;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.klickkit.phantom.R;
import com.klickkit.phantom.model.User;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

/**
 * Adapter to the mentions list shown to display the result of an '@' mention.
 */
public class MentionsAdapter extends RecyclerArrayAdapter<User, MentionsAdapter.MentionViewHolder> {

    /**
     * {@link Context}.
     */
    private final Context context;
    /**
     * {@link ForegroundColorSpan}.
     */
    private final ForegroundColorSpan colorSpan;
    /**
     * Current search string typed by the user.  It is used highlight the query in the
     * search results.  Ex: @bill.
     */
    private String currentQuery;

    public MentionsAdapter(final Context context) {
        this.context = context;
        final int orange = ContextCompat.getColor(context, R.color.mentions_default_color);
        this.colorSpan = new ForegroundColorSpan(orange);
    }

    /**
     * Setter for what user has queried.
     */
    public void setCurrentQuery(final String currentQuery) {
        if (StringUtils.isNotBlank(currentQuery)) {
            this.currentQuery = currentQuery.toLowerCase(Locale.US);
        }
    }

    /**
     * Create UI with views for user name and picture.
     */
    @Override
    public MentionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
        return new MentionViewHolder(view);
    }

    /**
     * Display user name and picture.
     */
    @Override
    public void onBindViewHolder(MentionViewHolder holder, int position) {
        final User mentionsUser = getItem(position);

        if (mentionsUser != null && StringUtils.isNotBlank(mentionsUser.getName())) {
            holder.name.setText(mentionsUser.getName(), TextView.BufferType.SPANNABLE);
            highlightSearchQueryInUserName(holder.name.getText());
            if (StringUtils.isNotBlank(mentionsUser.getAvatarURL())) {
                holder.imageView.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(mentionsUser.getAvatarURL())
                        .into(holder.imageView);
            } else {
                holder.imageView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Highlights the current search text in the mentions list.
     */
    private void highlightSearchQueryInUserName(CharSequence userName) {
        if (StringUtils.isNotBlank(currentQuery)) {
            int searchQueryLocation = userName.toString().toLowerCase(Locale.US).indexOf(currentQuery);

            if (searchQueryLocation != -1) {
                Spannable userNameSpannable = (Spannable) userName;
                userNameSpannable.setSpan(
                        colorSpan,
                        searchQueryLocation,
                        searchQueryLocation + currentQuery.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    /**
     * View holder for user.
     */
    static class MentionViewHolder extends RecyclerView.ViewHolder {
        final TextView name;
        final ImageView imageView;

        MentionViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.user_full_name);
            imageView = itemView.findViewById(R.id.user_picture);
        }
    }
}
