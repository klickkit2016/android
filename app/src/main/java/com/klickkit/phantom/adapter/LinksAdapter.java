package com.klickkit.phantom.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.klickkit.phantom.R;
import com.klickkit.phantom.model.Link;

import java.util.List;

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.ViewHolder> {
    private List<Link> links;
    private Context context;

    public LinksAdapter(Context context, List<Link> links) {
        this.links = links;
        this.context = context;
    }

    @Override
    public LinksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_link, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(LinksAdapter.ViewHolder viewHolder, int position) {
        final Link link = links.get(position);

        viewHolder.nameTV.setText(link.getLink_name());
        viewHolder.urlTV.setText(link.getUrl());

        viewHolder.linearLayout.setOnClickListener(v -> {
            String url = link.getUrl();
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(context.getResources().getColor(R.color.colorPrimary));
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, Uri.parse(url));
        });
    }

    @Override
    public int getItemCount() {
        return links.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        TextView nameTV;
        TextView urlTV;

        ViewHolder(View itemView) {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.link_ll);
            nameTV = itemView.findViewById(R.id.name);
            urlTV = itemView.findViewById(R.id.url);
        }
    }
}