package com.klickkit.phantom.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.klickkit.phantom.R;
import com.klickkit.phantom.model.Team;

import java.util.List;

public class TeamSpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<Team> teamList;

    public TeamSpinnerAdapter(Context context, List<Team> teamList) {
        this.context = context;
        this.teamList = teamList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.item_drawer_item, null);

        TextView title = view.findViewById(R.id.tv_drawer_item_title);
        ImageView dpIV = view.findViewById(R.id.iv_drawer_item_icon);

        final Team team = teamList.get(position);

        title.setText(team.getName());
        if (team.getDpURL() != null && !team.getDpURL().isEmpty())
            Glide.with(context).load(team.getDpURL())
                    .apply(RequestOptions.circleCropTransform())
                    .into(dpIV);

        return view;
    }

    @Override
    public int getCount() {
        return teamList.size();
    }

    @Nullable
    @Override
    public Team getItem(int position) {
        return teamList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
